/*this code is developed by Mahmoud Elmohr, Abdallah Mohammed, Ahmed El-Tawill for ( CAN-SAT II ) project 
which was held by Planterium Science center Bibliotheca Alexandrina and supported by UNISEC Egypt*/
/*this code demonstrates data aquistion for a gyroscope,accelerometer,humidity and temperature sensors 
and send the data by Xbee to a ground station, also to control parachut through servo motor */
/* to be mentioned we faced problem with GPS in hardware that's why we excluded it 
and also faced problems with presuure-altimeter sensor due to lack of analog pins
but both code are written exclusivley in seperate sketches*/



////declarations////
/// servo for parachut ///
#include <Servo.h>                                          //servo library
Servo parachutte;                                           //parachutte is controlled by servo
char reading;                                               //readings comming from Xbee for parachutte deployement


///gyro - acc ///
float x_ac, y_ac, z_ac;                                      //accelerations from accelerometer
float x_rot, y_rot;                                          //rotational velocities from gyroscope
float x_ang_ac,y_ang_ac;                                     //angles obtained by accelerometer
float x_ang,y_ang;                                           //final angles 
int xzero_ac, yzero_ac,zzero_ac,xzero_gyro, yzero_gyro;      //initial refrences
float g=9.81;                                                //gravity
unsigned long lastTime,now,timeChange;                       //time for integration process

///humidity ///
#include "DHT.h"                                             //DHT sensor library
DHT dht;                                                     


void setup()
{
   ///parachut servo ///
   parachutte.attach(3);


  ///gyro - acc ///
  Serial.begin(9600);                                       //serial communication for xbee   
  xzero_ac=analogRead(A0);
  yzero_ac=analogRead(A1);
  zzero_ac=analogRead(A2)-65;                               //-65 to remove the effect of gravity assuming that initialization is done while z axis is perpendicular to ground
  xzero_gyro=analogRead(A3);
  yzero_gyro=analogRead(A4);
  
  
  ///humidity ///
  dht.setup(2);                                          //humidity sensor initialization

void loop()
{
  

  now = millis();                                           //get the time now since the arduino is on
  timeChange = now - lastTime;                              // to calculate delta t for integration


  ///gyro - acc ///
  x_ac = (analogRead(A0)-xzero_ac)*g/67.518;      //real reading = reading-refrence , and the 61.44 is to convert readings into acceleration in m/s^2
                                                  //as 5v gives 1023 then 300mv gives 1023*.3/5 which is 61.44 and this value of reading equals 1 g whic is 9.8 m/s^2
  
  y_ac = (analogRead(A1)-yzero_ac)*g/67.518;      //same for x 
  z_ac = (analogRead(A2)-zzero_ac)*g/67.518;      //same for x 
  x_rot = (analogRead(A3)-xzero_gyro)/0.4096;     //real reading = reading-refrence , and the 0.4092 is to convert readings into angular velocity
                                                  //as 5v gives 1023 then 2mv gives 1023*.002/5 which is .4092 and this value of reading equals 1 degree/second
  y_rot = (analogRead(A4)-yzero_gyro)/0.4096;     //same for x 
 ///////////////////// 
  if ((x_rot<=3) && (x_rot>=-3))
 {
   x_rot=0;                                       // this to ellimnate small errors which may have effects when integerated
 } 
if ((y_rot<=3) && (y_rot>=-3))
 {
   y_rot=0;                                       // this to ellimnate small errors which may have effects when integerated
 }   
 /////////////////////////////////////   
   x_ang_ac = -1*atan2(x_ac, sqrt(y_ac*y_ac + z_ac*z_ac))*180.0/3.14;      //this equation is to caculae anges from accelerations , -1 due to position of accelerometer and gyroscope to make thier outputs in phase
    y_ang_ac = -1*atan2(y_ac, sqrt(x_ac*x_ac + z_ac*z_ac))*180.0/3.14;     //this equation is to caculae anges from accelerations , -1 due to position of accelerometer and gyroscope to make thier outputs in phase
  ////////////////////////////////////////  
     if(x_ang > 90)
            x_ang_ac=180-x_ang_ac;               //to keep angles from accelerometer between -90 and 90 to avoid problems
     else if(x_ang < -90)
            x_ang_ac=-180-x_ang_ac;
     if(y_ang > 90)
            y_ang_ac=180-y_ang_ac;
     else if(y_ang < -90)
            y_ang_ac=-180-y_ang_ac;
 ///////////////////////////    
  x_ang = (x_ang + x_rot*timeChange/1000)*.9 + x_ang_ac*.1;              //complementry filter to have accurate angle from both acceleromete and gyroscope
  y_ang = (y_ang +y_rot*timeChange/1000)*.9+ y_ang_ac*.1;
  /////////////////////////////////////
  if (x_ang>180)                                                         //to keep final angles between -180 and 180 
        x_ang = -360+ x_ang ;
  else if (x_ang<-180)
        x_ang = 360+ x_ang ;
  if (y_ang>180)
        y_ang = -360+ y_ang ;
  else if (y_ang<-180)
        y_ang = 360+ y_ang ;
  /////////////////////////
  if ((x_ang<=1) && (x_ang>=-1))
 {
   x_ang=0;                                      // this to ellimnate small errors which may have effects when integerated
 } 
if ((y_ang<=1) && (y_ang>=-1))
 {
   y_ang=0;                                      // this to ellimnate small errors which may have effects when integerated
 }  
 
 
   ////humidity /////////////////

 int humidity = dht.getHumidity();               //to get humidity
 int temperature = dht.getTemperature();         //to get temperature
  
  ////gyro - acc  (print) /////////////////
  //Serial.print("angels of x, y:   ");
  Serial.print(now, DEC);
  Serial.print(",");       
  Serial.print(x_ang, DEC);    
  Serial.print(",");       
  Serial.print(y_ang, DEC);  
  
  ////humidity & temp (print) /////////////////

  Serial.print(",");
  Serial.print(temperature);
  Serial.print(",");
  Serial.print(humidity);
  
   Serial.print(",");
  Serial.print("anything");                      //this was a protocol for transmission of data with ground station
  Serial.print(",");
  Serial.println("anything");
  
  
  lastTime = now;
  reading=Serial.read();
     if ((reading=='p') || (reading =='P'))
  {
          parachutte.write(90);
          delay(15);
          parachutte.write(0);
          delay(15);  
      }
      
}



