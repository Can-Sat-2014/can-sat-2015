﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb; 

namespace CAN_SAT
{
    public partial class Form1 : Form
    {
      
        //static string strDSN = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\mouhamed\Documents\Visual Studio 2010\Projects\CAN-SAT\CAN-SAT\bin\Debug\CAN-SAT.mdb;Persist Security Info=False;";
        //constant string for establshing database connection
        static string strDSN = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\mouhamed\Documents\Visual Studio 2010\Projects\CAN-SAT\CAN-SAT\bin\Debug\CAN-SAT.accdb;Persist Security Info=False;";
       //instant oledb object
        OleDbConnection myConn = new OleDbConnection(strDSN);
        
        
       

        public Form1()
        {
            // function call for gui initializing by VS
            InitializeComponent();
        }

        //fit the component according to resize action
        private void Form1_Resize(object sender, EventArgs e)
        {
            tabControl1.Dock = DockStyle.Fill;
            
        }

        //establishing connection over com port selected in combobox
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (serialPort1.IsOpen)
                    serialPort1.Close();
                serialPort1.PortName = comboBox1.Text;
                serialPort1.Open();
                label1.Text = "connected";
                button1.Enabled = false;
                button2.Enabled = true;
                comboBox1.Enabled = false;
                textBox1.Focus();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "error");
            }
        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_CAN_SATDataSet4.meassures' table. You can move, or remove it, as needed.
            this.meassuresTableAdapter2.Fill(this._CAN_SATDataSet4.meassures);
            
            // get com ports availabe into combo box
            string[] serialports = System.IO.Ports.SerialPort.GetPortNames();
            for (int i = 0; i <= serialports.Length - 1; i++)
            {
                comboBox1.Items.Add(serialports[i]);
            }
            button2.Enabled = false;

            
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                Invoke(new Action(() => data_recieved()));//function call to manipulate the data received in the same thread in which object created 
                
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        // closing the connection over com port
        private void button2_Click(object sender, EventArgs e)
        {

            serialPort1.Close();
            label1.Text = "disconnected";
            button1.Enabled = true;
            button2.Enabled = false;
            comboBox1.Enabled = true;
        }

        // sending data and commands in textbox over com port
        private void button3_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "")
            {
                try
                {
                    serialPort1.WriteLine( textBox1.Text);
                    richTextBox1.AppendText("This Port:" + textBox1.Text + Environment.NewLine);
                    textBox1.Text = string.Empty;
                    textBox1.Focus();
                    richTextBox1.ScrollToCaret();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                return;
            }
        
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)Keys.Enter)
            {
                e.KeyChar = '\0';
                button3_Click(null, null);

            }
        }

        //reading data deliverd on com port and inserting it into microsoft access database
        void data_recieved()
        {
            try
            {
                string port_data = serialPort1.ReadLine();
                string[] data = port_data.Split(',');
                foreach (string word in data)
                {
                    richTextBox1.AppendText(word);
                }
                
                //string strSQL = "INSERT INTO orientation(x, y ) VALUES(" + data[0] + "," + data[1] + ")";
                string strSQL = "INSERT INTO meassures( x_orintation, y_orintation, temp, humedity) VALUES(" + data[1] + "," + data[2] + "," + data[3] + "," + data[4] +  ")";


                OleDbCommand mycmd = new OleDbCommand(strSQL, myConn);
                myConn.Open();
                mycmd.ExecuteNonQuery();
                meassuresDataGridView.Update();
                myConn.Close();

                richTextBox1.AppendText("remote port:" + port_data + Environment.NewLine);
                richTextBox1.ScrollToCaret();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        
    }
}
