#include<SoftwareSerial.h>

SoftwareSerial myser(6,7);
int c[100],z=1;
char printmychr(int);

void setup()
{
  myser.begin(9600);
  Serial.begin(115200);
  int i;
  for(i=0;i<100;i++)
    c[i]=0;
}

void loop()
{
  int i=0,x;
  bool myf=false;
  unsigned long start=millis();
  while(millis()-start<1000)
  {
    while(myser.available())
    {
      c[i]=myser.read();
      if(c[i] == 0xA && c[i-1]== 0xD)break;
      i++;
    }
  }
  Serial.print("Line ");
  Serial.print(z++);
  Serial.println(" :");
  for(x=0;c[x]!=0xA && c[x-1]!=0xD;x++)
  {
    Serial.print(printmychr(c[x]));
    Serial.print(" ");
  }
  Serial.println(" ");
}

char printmychr(int c)
{
  switch(c)
  {
    case 0x30: return '0';break;
    case 0x31: return '1';break;
    case 0x32: return '2';break;
    case 0x33: return '3';break;
    case 0x34: return '4';break;
    case 0x35: return '5';break;
    case 0x36: return '6';break;
    case 0x37: return '7';break;
    case 0x38: return '8';break;
    case 0x39: return '9';break;
    case 0x41: return 'A';break;
    case 0x42: return 'B';break;
    case 0x43: return 'C';break;
    case 0x44: return 'D';break;
    case 0x45: return 'E';break;
    case 0x46: return 'F';break;
    case 0x47: return 'G';break;
    case 0x48: return 'H';break;
    case 0x49: return 'I';break;
    case 0x4A: return 'J';break;
    case 0x4B: return 'K';break;
    case 0x4C: return 'L';break;
    case 0x4D: return 'M';break;
    case 0x4E: return 'N';break;
    case 0x4F: return 'O';break;
    case 0x50: return 'P';break;
    case 0x51: return 'Q';break;
    case 0x52: return 'R';break;
    case 0x53: return 'S';break;
    case 0x54: return 'T';break;
    case 0x55: return 'U';break;
    case 0x56: return 'V';break;
    case 0x57: return 'W';break;
    case 0x58: return 'X';break;
    case 0x59: return 'Y';break;
    case 0x5A: return 'Z';break;
    case 0x21: return '!';break;
    case 0x22: return '"';break;
    case 0x23: return '#';break;
    case 0x24: return '$';break;
    case 0x25: return '%';break;
    case 0x26: return '&';break;
    case 0x27: return '\'';break;
    case 0x28: return '(';break;
    case 0x29: return ')';break;
    case 0x2A: return '*';break;
    case 0x2B: return '+';break;
    case 0x2C: return ',';break;
    case 0x2D: return '-';break;
    case 0x2E: return '.';break;
    case 0x2F: return '/';break;
    default: return c;
  }
}
